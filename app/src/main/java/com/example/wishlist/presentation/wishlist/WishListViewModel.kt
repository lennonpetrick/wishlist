package com.example.wishlist.presentation.wishlist

import androidx.lifecycle.LifecycleOwner
import com.example.wishlist.data.repositories.Repository
import com.example.wishlist.domain.models.Offering
import com.example.wishlist.domain.usecases.AddRemoveWishListUseCase
import com.example.wishlist.presentation.BaseViewModel
import com.example.wishlist.presentation.OfferingView
import com.example.wishlist.presentation.OfferingViewMapper
import com.example.wishlist.presentation.ViewState
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable

internal class WishListViewModel(
    private val repository: Repository,
    private val useCase: AddRemoveWishListUseCase,
    private val mapper: OfferingViewMapper,
    workScheduler: Scheduler,
    uiScheduler: Scheduler,
    compositeDisposable: CompositeDisposable
) : BaseViewModel(workScheduler, uiScheduler, compositeDisposable) {

    private lateinit var offerings: List<Offering>

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        fetchWishList()
    }

    private fun fetchWishList() {
        postState(WishListState.Loading)
        getWishOfferingList()
            .applySchedulers()
            .subscribe(::postState) { postState(WishListState.Error) }
            .disposeInOnCleared()
    }

    fun removeWishList(view: OfferingView) {
        if (!::offerings.isInitialized) return

        val offering = offerings.first { it.id == view.id }
        useCase.run(offering)
            .flatMap { getWishOfferingList() }
            .applySchedulers()
            .subscribe(::postState) { postState(WishListState.Error) }
            .disposeInOnCleared()
    }

    private fun getWishOfferingList(): Single<WishListState.WishOfferingList> {
        return repository.getWishOfferingList()
            .doOnSuccess { offerings = it }
            .flattenAsObservable { it }
            .map(mapper::convert)
            .toList()
            .map(WishListState::WishOfferingList)
    }

}

internal sealed class WishListState: ViewState {
    internal object Loading : WishListState()
    internal object Error : WishListState()
    internal class WishOfferingList(val offerings: List<OfferingView>) : WishListState()
}