package com.example.wishlist.presentation.offeringList

import androidx.lifecycle.LifecycleOwner
import com.example.wishlist.data.repositories.Repository
import com.example.wishlist.domain.models.Offering
import com.example.wishlist.domain.usecases.AddRemoveWishListUseCase
import com.example.wishlist.presentation.BaseViewModel
import com.example.wishlist.presentation.OfferingView
import com.example.wishlist.presentation.OfferingViewMapper
import com.example.wishlist.presentation.ViewState
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.internal.functions.Functions
import io.reactivex.rxjava3.kotlin.subscribeBy

internal class OfferingListViewModel(
    private val repository: Repository,
    private val useCase: AddRemoveWishListUseCase,
    private val mapper: OfferingViewMapper,
    workScheduler: Scheduler,
    uiScheduler: Scheduler,
    compositeDisposable: CompositeDisposable
) : BaseViewModel(workScheduler, uiScheduler, compositeDisposable) {

    private val offerings = mutableListOf<Offering>()

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        fetchOfferingList()
        subscribeToOfferingUpdates()
    }

    private fun subscribeToOfferingUpdates() {
        repository.getOfferingUpdates()
            .map { newOffering ->
                val oldOffering = offerings.firstOrNull { it.id == newOffering.id }
                oldOffering?.let { updateAndReturnList(it, newOffering) } ?: emptyList()
            }
            .filter { it.isNotEmpty() }
            .map(OfferingListState::OfferingList)
            .applySchedulers()
            .subscribe(::postState) { postState(OfferingListState.Error) }
            .disposeInOnCleared()
    }

    private fun fetchOfferingList() {
        postState(OfferingListState.Loading)
        repository.getOfferingList()
            .doOnSuccess {
                offerings.clear()
                offerings.addAll(it)
            }
            .flattenAsObservable { it }
            .map(mapper::convert)
            .toList()
            .map(OfferingListState::OfferingList)
            .applySchedulers()
            .subscribe(::postState) { postState(OfferingListState.Error) }
            .disposeInOnCleared()
    }

    fun addRemoveWishList(view: OfferingView) {
        val offering = offerings.first { it.id == view.id }
        useCase.run(offering)
            .ignoreElement()
            .applySchedulers()
            .subscribeBy(
                onComplete = { Functions.EMPTY_ACTION },
                onError = { postState(OfferingListState.Error) }
            ).disposeInOnCleared()
    }

    private fun updateAndReturnList(oldItem: Offering, newItem: Offering): List<OfferingView> {
        val index = offerings.indexOf(oldItem)
        offerings[index] = newItem
        return offerings.map(mapper::convert)
    }

}

internal sealed class OfferingListState: ViewState {
    internal object Loading : OfferingListState()
    internal object Error : OfferingListState()
    internal class OfferingList(val offerings: List<OfferingView>) : OfferingListState()
}