package com.example.wishlist.presentation

import com.example.wishlist.R
import com.example.wishlist.domain.models.Offering
import com.example.wishlist.presentation.utils.DateFormatter
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

internal class OfferingViewMapper @Inject constructor(private val dateFormatter: DateFormatter) {

    fun convert(offering: Offering): OfferingView {
        with(offering) {
            val wished = when (wished) {
                true -> R.drawable.ic_bookmark_filled
                false -> R.drawable.ic_bookmark
            }

            val price = formatPrice(price, currencyCode)
            val rating = starsRating?.formatRating()
            val dateRange = when {
                startDate != null && endDate != null -> dateFormatter.formatDateRange(startDate, endDate)
                else -> null
            }

            return OfferingView(offering.id, wished, offering.name,
                offering.image, price, rating, offering.location, dateRange)
        }
    }

    private fun formatPrice(price: Double, currencyCode: String): String {
        val currencySymbol = Currency.getInstance(currencyCode).symbol
        val numberFormat = NumberFormat.getCurrencyInstance()
        val decimalFormatSymbols = (numberFormat as DecimalFormat).decimalFormatSymbols
        decimalFormatSymbols.currencySymbol = currencySymbol
        numberFormat.decimalFormatSymbols = decimalFormatSymbols
        numberFormat.maximumFractionDigits = 2
        return numberFormat.format(price)
    }

    private fun Double.formatRating(): String = "%.${2}f".format(this).plus(" / 5")

}