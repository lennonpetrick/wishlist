package com.example.wishlist.presentation.utils

import android.view.View
import android.widget.TextView

fun TextView.setTextOrGone(text: String?) {
    when (text) {
        null -> this.visibility = View.GONE
        else -> {
            this.visibility = View.VISIBLE
            this.text = text
        }
    }
}