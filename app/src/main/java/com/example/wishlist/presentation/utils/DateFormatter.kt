package com.example.wishlist.presentation.utils

import android.text.format.DateUtils
import java.util.*
import javax.inject.Inject

internal class DateFormatter @Inject constructor() {

    private val builder by lazy { StringBuilder() }
    private val formatter by lazy { Formatter(builder, Locale.getDefault()) }

    fun formatDateRange(startDate: Date, endDate: Date): String {
        builder.setLength(0)
        return DateUtils.formatDateRange(null, formatter, startDate.time, endDate.time, 0).toString()
    }

}