package com.example.wishlist.presentation.wishlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.example.wishlist.data.repositories.Repository
import com.example.wishlist.di.*
import com.example.wishlist.domain.usecases.AddRemoveWishListUseCase
import com.example.wishlist.presentation.OfferingViewMapper
import dagger.Component
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

@Module
internal class WishListModule(private val owner: ViewModelStoreOwner) {

    @Provides
    fun providesWishListViewModel(factory: WishListViewModelFactory): WishListViewModel {
        return ViewModelProvider(owner, factory).get(WishListViewModel::class.java)
    }

    @Provides
    internal fun providesCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

}

@FeatureScope
@Component(dependencies = [AppComponent::class], modules = [WishListModule::class])
internal interface WishListComponent {
    fun inject(activity: WishListActivity)

    companion object {
        fun init(activity: WishListActivity) {
            DaggerWishListComponent.builder()
                .appComponent(Components.appComponent())
                .wishListModule(WishListModule(activity))
                .build()
                .inject(activity)
        }
    }
}

internal class WishListViewModelFactory @Inject constructor(
    private val repository: Repository,
    private val useCase: AddRemoveWishListUseCase,
    private val mapper: OfferingViewMapper,
    @IOScheduler private val workScheduler: Scheduler,
    @UIScheduler private val uiScheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WishListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return WishListViewModel(repository, useCase, mapper, workScheduler, uiScheduler, compositeDisposable) as T
        }
        throw IllegalStateException("Unknown ViewModel class")
    }
}