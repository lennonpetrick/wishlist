package com.example.wishlist.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.wishlist.databinding.LayoutOfferingsAdapterBinding
import com.example.wishlist.presentation.utils.setTextOrGone
import com.squareup.picasso.Picasso

internal class OfferingListAdapter(private val listener: (OfferingView) -> Unit) : RecyclerView.Adapter<OfferingListViewHolder>() {

    var offerings: List<OfferingView> = emptyList()
        set(value) {
            val oldList = field
            field = value
            DiffUtil.calculateDiff(OfferingDiffUtil(oldList, field))
                .dispatchUpdatesTo(this)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferingListViewHolder {
        return OfferingListViewHolder(LayoutOfferingsAdapterBinding.inflate(
            LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: OfferingListViewHolder, position: Int) {
        holder.bind(offerings[position], listener)
    }

    override fun getItemCount(): Int {
        return offerings.size
    }

    class OfferingDiffUtil(private val oldList: List<OfferingView>,
                           private val newList: List<OfferingView>) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

    }
}

internal class OfferingListViewHolder(private val binding: LayoutOfferingsAdapterBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(offering: OfferingView, listener: (OfferingView) -> Unit) {
        with(binding) {
            Picasso.get()
                .load(offering.image)
                .fit()
                .into(image)

            bookmark.setOnClickListener { listener.invoke(offering) }
            bookmark.setImageResource(offering.wished)
            name.text = offering.name
            location.setTextOrGone(offering.location)
            date.setTextOrGone(offering.dateRange)
            price.text = offering.price

            ratingGroup.visibility = offering.rating?.let {
                rating.text = it
                View.VISIBLE
            } ?: View.GONE
        }
    }

}