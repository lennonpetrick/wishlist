package com.example.wishlist.presentation

import androidx.annotation.DrawableRes

internal data class OfferingView(
    val id: String,
    @DrawableRes val wished: Int,
    val name: String,
    val image: String,
    val price: String,
    val rating: String?,
    val location: String?,
    val dateRange: String?
)