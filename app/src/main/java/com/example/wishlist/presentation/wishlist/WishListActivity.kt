package com.example.wishlist.presentation.wishlist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wishlist.R
import com.example.wishlist.databinding.ActivityWishListBinding
import com.example.wishlist.presentation.OfferingListAdapter
import com.example.wishlist.presentation.OfferingView
import javax.inject.Inject

class WishListActivity : AppCompatActivity() {

    companion object {
        fun startActivity(context: Context) {
            Intent(context, WishListActivity::class.java).apply {
                context.startActivity(this)
            }
        }
    }

    @Inject
    internal lateinit var viewModel: WishListViewModel

    private lateinit var binding: ActivityWishListBinding
    private lateinit var offeringListAdapter: OfferingListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWishListBinding.inflate(LayoutInflater.from(this)).apply { setContentView(root) }
        WishListComponent.init(this)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setUpRecyclerView()

        viewModel.state<WishListState>().observe(this, { onStateChanges(it) })
        lifecycle.addObserver(viewModel)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUpRecyclerView() {
        offeringListAdapter = OfferingListAdapter { viewModel.removeWishList(it) }
        with(binding.offerings) {
            adapter = offeringListAdapter

            addItemDecoration(DividerItemDecoration(this@WishListActivity, DividerItemDecoration.VERTICAL).apply {
                AppCompatResources.getDrawable(this@WishListActivity,
                    R.drawable.drawable_divider_transparent)?.apply { setDrawable(this) }
            })

            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(this@WishListActivity)
        }
    }

    private fun onStateChanges(state: WishListState) {
        if (state != WishListState.Loading) enableLoading(false)

        when (state) {
            is WishListState.Loading -> enableLoading(true)
            is WishListState.Error -> showDefaultError()
            is WishListState.WishOfferingList -> displayOfferings(state.offerings)
        }
    }

    private fun enableLoading(param: Boolean) {
        if (param) {
            binding.progress.visibility = View.VISIBLE
            binding.offerings.visibility = View.GONE
        } else {
            binding.progress.visibility = View.GONE
            binding.offerings.visibility = View.VISIBLE
        }
    }

    private fun displayOfferings(offerings: List<OfferingView>) {
        offeringListAdapter.offerings = offerings
    }

    private fun showDefaultError() {
        Toast.makeText(this, getString(R.string.default_error_message), Toast.LENGTH_LONG).show()
    }
}