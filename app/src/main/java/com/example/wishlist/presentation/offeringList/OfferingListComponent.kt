package com.example.wishlist.presentation.offeringList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.example.wishlist.data.repositories.Repository
import com.example.wishlist.di.*
import com.example.wishlist.domain.usecases.AddRemoveWishListUseCase
import com.example.wishlist.presentation.OfferingViewMapper
import dagger.Component
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

@Module
internal class OfferingListModule(private val owner: ViewModelStoreOwner) {

    @Provides
    fun providesOfferingListViewModel(factory: OfferingListViewModelFactory): OfferingListViewModel {
        return ViewModelProvider(owner, factory).get(OfferingListViewModel::class.java)
    }

    @Provides
    internal fun providesCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

}

@FeatureScope
@Component(dependencies = [AppComponent::class], modules = [OfferingListModule::class])
internal interface OfferingListComponent {
    fun inject(activity: OfferingListActivity)

    companion object {
        fun init(activity: OfferingListActivity) {
            DaggerOfferingListComponent.builder()
                .appComponent(Components.appComponent())
                .offeringListModule(OfferingListModule(activity))
                .build()
                .inject(activity)
        }
    }
}

internal class OfferingListViewModelFactory @Inject constructor(
    private val repository: Repository,
    private val useCase: AddRemoveWishListUseCase,
    private val mapper: OfferingViewMapper,
    @IOScheduler private val workScheduler: Scheduler,
    @UIScheduler private val uiScheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OfferingListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return OfferingListViewModel(repository, useCase, mapper, workScheduler, uiScheduler, compositeDisposable) as T
        }
        throw IllegalStateException("Unknown ViewModel class")
    }
}