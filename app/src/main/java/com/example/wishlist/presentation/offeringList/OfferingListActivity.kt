package com.example.wishlist.presentation.offeringList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wishlist.R
import com.example.wishlist.databinding.ActivityOfferingListBinding
import com.example.wishlist.presentation.OfferingListAdapter
import com.example.wishlist.presentation.OfferingView
import com.example.wishlist.presentation.wishlist.WishListActivity
import javax.inject.Inject

class OfferingListActivity : AppCompatActivity() {

    @Inject
    internal lateinit var viewModel: OfferingListViewModel

    private lateinit var binding: ActivityOfferingListBinding
    private lateinit var offeringListAdapter: OfferingListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOfferingListBinding.inflate(LayoutInflater.from(this)).apply { setContentView(root) }
        OfferingListComponent.init(this)
        setSupportActionBar(binding.toolbar)
        setUpRecyclerView()

        viewModel.state<OfferingListState>().observe(this, { onStateChanges(it) })
        lifecycle.addObserver(viewModel)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.wishlist) {
            WishListActivity.startActivity(this)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUpRecyclerView() {
        offeringListAdapter = OfferingListAdapter { viewModel.addRemoveWishList(it) }
        with(binding.offerings) {
            adapter = offeringListAdapter

            addItemDecoration(DividerItemDecoration(this@OfferingListActivity, DividerItemDecoration.VERTICAL).apply {
                AppCompatResources.getDrawable(this@OfferingListActivity,
                    R.drawable.drawable_divider_transparent)?.apply { setDrawable(this) }
            })

            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(this@OfferingListActivity)
        }
    }

    private fun onStateChanges(state: OfferingListState) {
        if (state != OfferingListState.Loading) enableLoading(false)

        when (state) {
            is OfferingListState.Loading -> enableLoading(true)
            is OfferingListState.Error -> showDefaultError()
            is OfferingListState.OfferingList -> displayOfferings(state.offerings)
        }
    }

    private fun enableLoading(param: Boolean) {
        if (param) {
            binding.progress.visibility = View.VISIBLE
            binding.offerings.visibility = View.GONE
        } else {
            binding.progress.visibility = View.GONE
            binding.offerings.visibility = View.VISIBLE
        }
    }

    private fun displayOfferings(offerings: List<OfferingView>) {
        offeringListAdapter.offerings = offerings
    }

    private fun showDefaultError() {
        Toast.makeText(this, getString(R.string.default_error_message), Toast.LENGTH_LONG).show()
    }
}