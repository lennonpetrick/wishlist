package com.example.wishlist.data.datasources.network

import com.example.wishlist.data.entities.OfferingEntity
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

internal class NetworkDataSource @Inject constructor(private val service: ApiService) {

    fun getOfferingList(): Single<List<OfferingEntity>> {
        return service.getOfferingList()
            .map { it.items }
    }

}