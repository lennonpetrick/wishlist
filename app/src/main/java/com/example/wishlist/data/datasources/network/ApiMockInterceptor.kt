package com.example.wishlist.data.datasources.network

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import java.io.BufferedReader
import java.io.InputStreamReader

class ApiMockInterceptor(private val applicationContext: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url
        return when (url.encodedPath) {
            "/offerings.json" -> {
                val response = readJsonFile()
                Response.Builder()
                    .code(200)
                    .message(response)
                    .request(chain.request())
                    .protocol(Protocol.HTTP_1_1)
                    .body(response.toResponseBody())
                    .build()
            }
            else -> chain.proceed(chain.request())
        }
    }

    private fun readJsonFile(): String {
        val bufferedReader = BufferedReader(InputStreamReader(applicationContext.assets.open("offerings.json")))
        val json = bufferedReader.readText()
        bufferedReader.close()
        return json
    }
}