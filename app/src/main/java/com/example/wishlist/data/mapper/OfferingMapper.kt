package com.example.wishlist.data.mapper

import com.example.wishlist.data.entities.OfferingEntity
import com.example.wishlist.domain.models.Offering
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

internal class OfferingMapper @Inject constructor() {

    private val dateFormatter by lazy {
        SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    }

    fun convert(entity: OfferingEntity): Offering {
        val price = entity.price.toDouble()
        val startDate = entity.startDate?.let { dateFormatter.parse(it) }
        val endDate = entity.endDate?.let { dateFormatter.parse(it) }
        return Offering(
            entity.id, entity.wished, Offering.Type.valueOf(entity.type), entity.name,
            entity.image, entity.currencyCode, price, entity.starsRating,
            entity.location, startDate, endDate
        )
    }

    fun convert(model: Offering): OfferingEntity {
        val price = model.price.toString()
        val startDate = model.startDate?.let { dateFormatter.format(it) }
        val endDate = model.endDate?.let { dateFormatter.format(it) }
        return OfferingEntity(
            model.id, model.wished, model.type.toString(), model.name,
            model.image, model.currencyCode, price, model.starsRating,
            model.location, startDate, endDate
        )
    }
}