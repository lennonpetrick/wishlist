package com.example.wishlist.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

internal class OfferingEntityWrapper(val items: List<OfferingEntity>)

@Entity
internal data class OfferingEntity(
    @PrimaryKey
    val id: String,
    val wished: Boolean,
    val type: String,
    val name: String,
    val image: String,

    @SerializedName("price_currency_code")
    val currencyCode: String,

    val price: String,
    val starsRating: Double?,
    val location: String?,
    val startDate: String?,
    val endDate: String?,
)