package com.example.wishlist.data.datasources.local

import com.example.wishlist.data.entities.OfferingEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

internal class LocalDataSource @Inject constructor(private val offeringDao: OfferingDao) {

    private val offeringSubject = PublishSubject.create<OfferingEntity>()

    fun getOfferingList(): Single<List<OfferingEntity>> {
        return offeringDao.getOfferingList()
    }

    fun getWishOfferingList(): Single<List<OfferingEntity>> {
        return offeringDao.getWishOfferingList()
    }

    fun saveOfferingList(list: List<OfferingEntity>): Completable {
        return offeringDao.saveOfferingList(list)
    }

    fun updateOffering(entity: OfferingEntity): Completable {
        return offeringDao.updateOffering(entity)
            .doOnComplete { offeringSubject.onNext(entity) }
    }

    fun getOfferingUpdates(): Observable<OfferingEntity> = offeringSubject

}