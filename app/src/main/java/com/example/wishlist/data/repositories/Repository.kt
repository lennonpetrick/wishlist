package com.example.wishlist.data.repositories

import com.example.wishlist.data.datasources.local.LocalDataSource
import com.example.wishlist.data.datasources.network.NetworkDataSource
import com.example.wishlist.data.entities.OfferingEntity
import com.example.wishlist.data.mapper.OfferingMapper
import com.example.wishlist.domain.models.Offering
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class Repository @Inject constructor(
    private val networkDataSource: NetworkDataSource,
    private val localDataSource: LocalDataSource,
    private val mapper: OfferingMapper,
) {

    fun getOfferingList(): Single<List<Offering>> {
        return getOfferingsOnNetwork()
            .onErrorResumeNext { localDataSource.getOfferingList() }
            .map()
    }

    fun getOfferingUpdates(): Observable<Offering> {
        return localDataSource.getOfferingUpdates()
            .map(mapper::convert)
    }

    fun getWishOfferingList(): Single<List<Offering>> {
        return localDataSource.getWishOfferingList().map()
    }

    fun updateOffering(offering: Offering): Completable {
        val entity = mapper.convert(offering)
        return localDataSource.updateOffering(entity)
    }

    private fun getOfferingsOnNetwork(): Single<List<OfferingEntity>> {
        return networkDataSource.getOfferingList()
            .flatMap { localDataSource.saveOfferingList(it).toSingleDefault(it) }
    }

    private fun Single<List<OfferingEntity>>.map(): Single<List<Offering>> {
        return this.flattenAsObservable { it }
            .map(mapper::convert)
            .toList()
    }

}