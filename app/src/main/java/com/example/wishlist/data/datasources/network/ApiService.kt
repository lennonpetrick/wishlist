package com.example.wishlist.data.datasources.network

import com.example.wishlist.data.entities.OfferingEntityWrapper
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

internal interface ApiService {

    @GET("offerings.json")
    fun getOfferingList(): Single<OfferingEntityWrapper>

}