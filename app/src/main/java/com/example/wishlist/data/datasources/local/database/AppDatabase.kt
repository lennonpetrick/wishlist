package com.example.wishlist.data.datasources.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.wishlist.data.datasources.local.OfferingDao
import com.example.wishlist.data.entities.OfferingEntity

@Database(entities = [OfferingEntity::class], version = 1)
internal abstract class AppDatabase : RoomDatabase() {
    abstract fun offeringDao(): OfferingDao
}