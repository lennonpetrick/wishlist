package com.example.wishlist.data.datasources.local

import androidx.room.*
import com.example.wishlist.data.entities.OfferingEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

@Dao
internal interface OfferingDao {

    @Query("SELECT * FROM OfferingEntity")
    fun getOfferingList(): Single<List<OfferingEntity>>

    @Query("SELECT * FROM OfferingEntity WHERE wished = 1")
    fun getWishOfferingList(): Single<List<OfferingEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveOfferingList(list: List<OfferingEntity>): Completable

    @Update(entity = OfferingEntity::class, onConflict = OnConflictStrategy.REPLACE)
    fun updateOffering(entity: OfferingEntity): Completable

}