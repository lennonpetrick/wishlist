package com.example.wishlist.domain.usecases

import com.example.wishlist.domain.models.Offering
import com.example.wishlist.data.repositories.Repository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

internal class AddRemoveWishListUseCase @Inject constructor(private val repository: Repository) {

    fun run(offering: Offering): Single<Offering> {
        val newOffering = offering.copy(wished = !offering.wished)
        return repository.updateOffering(newOffering)
            .toSingleDefault(newOffering)
    }

}