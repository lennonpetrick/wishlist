package com.example.wishlist.domain.models

import java.util.*

internal data class Offering(
    val id: String,
    val wished: Boolean,
    val type: Type,
    val name: String,
    val image: String,
    val currencyCode: String,
    val price: Double,
    val starsRating: Double?,
    val location: String?,
    val startDate: Date?,
    val endDate: Date?,
) {

    enum class Type {
        VENUE,
        EXHIBITION
    }

}