package com.example.wishlist.di

import android.content.Context
import com.example.wishlist.di.modules.DatabaseModule
import com.example.wishlist.di.modules.NetworkModule
import com.example.wishlist.di.modules.SchedulerModule

internal object Components {

    private lateinit var appComponent: AppComponent

    fun init(applicationContext: Context) {
        appComponent = DaggerAppComponent.builder()
            .networkModule(NetworkModule(applicationContext))
            .schedulerModule(SchedulerModule())
            .databaseModule(DatabaseModule(applicationContext))
            .build()
    }

    fun appComponent() = appComponent

}