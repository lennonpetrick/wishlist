package com.example.wishlist.di

import javax.inject.Scope

@Scope
annotation class FeatureScope