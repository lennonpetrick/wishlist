package com.example.wishlist.di

import com.example.wishlist.data.repositories.Repository
import com.example.wishlist.di.modules.DatabaseModule
import com.example.wishlist.di.modules.NetworkModule
import com.example.wishlist.di.modules.SchedulerModule
import dagger.Component
import io.reactivex.rxjava3.core.Scheduler
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, DatabaseModule::class, SchedulerModule::class])
internal interface AppComponent {

    @IOScheduler
    fun ioScheduler(): Scheduler

    @UIScheduler
    fun uiScheduler(): Scheduler

    fun repository(): Repository

}