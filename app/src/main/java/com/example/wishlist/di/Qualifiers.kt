package com.example.wishlist.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
internal annotation class IOScheduler

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
internal annotation class UIScheduler