package com.example.wishlist.di.modules

import android.content.Context
import androidx.room.Room
import com.example.wishlist.data.datasources.local.OfferingDao
import com.example.wishlist.data.datasources.local.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class DatabaseModule(private val applicationContext: Context) {

    @Provides
    @Singleton
    fun providesAppDatabase(): AppDatabase {
        return Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "wishlist.db")
            .build()
    }

    @Provides
    @Singleton
    fun providesOfferingDao(database: AppDatabase): OfferingDao {
        return database.offeringDao()
    }
}