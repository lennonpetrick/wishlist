package com.example.wishlist

import android.app.Application
import com.example.wishlist.di.Components

internal class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Components.init(this)
    }

}