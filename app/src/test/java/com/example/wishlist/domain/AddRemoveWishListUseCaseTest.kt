package com.example.wishlist.domain

import com.example.wishlist.data.TestFactory
import com.example.wishlist.data.repositories.Repository
import com.example.wishlist.domain.models.Offering
import com.example.wishlist.domain.usecases.AddRemoveWishListUseCase
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.rxjava3.core.Completable
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
internal class AddRemoveWishListUseCaseTest {

    @Mock
    private lateinit var repository: Repository

    @InjectMocks
    private lateinit var subject: AddRemoveWishListUseCase

    @Test
    fun `When offering is wished true, then it returns a offering updated with false`() {
        val offering = TestFactory.createOfferingModel(wished = true)
        whenever(repository.updateOffering(any())).thenReturn(Completable.complete())

        subject.run(offering)
            .test()
            .assertNoErrors()
            .assertValue { it is Offering && !it.wished }
    }

    @Test
    fun `When offering is wished false, then it returns a offering updated with true`() {
        val offering = TestFactory.createOfferingModel(wished = false)
        whenever(repository.updateOffering(any())).thenReturn(Completable.complete())

        subject.run(offering)
            .test()
            .assertNoErrors()
            .assertValue { it is Offering && it.wished }
    }

}