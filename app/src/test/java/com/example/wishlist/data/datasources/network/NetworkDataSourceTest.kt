package com.example.wishlist.data.datasources.network

import com.example.wishlist.data.entities.OfferingEntity
import com.example.wishlist.data.entities.OfferingEntityWrapper
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.rxjava3.core.Single
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
internal class NetworkDataSourceTest {

    @Mock
    private lateinit var service: ApiService

    @InjectMocks
    private lateinit var subject: NetworkDataSource

    @Test
    fun `When fetching offerings, then a list is returned`() {
        val offerings = listOf(mock<OfferingEntity>())
        whenever(service.getOfferingList()).thenReturn(Single.just(OfferingEntityWrapper(offerings)))

        subject.getOfferingList()
            .test()
            .assertNoErrors()
            .assertValue(offerings)
    }

    @Test
    fun `When fetching offerings fails, then an error is returned`() {
        val exception = Exception()
        whenever(service.getOfferingList()).thenReturn(Single.error(exception))

        subject.getOfferingList()
            .test()
            .assertNoValues()
            .assertError(exception)
    }

}