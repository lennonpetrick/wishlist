package com.example.wishlist.data

import com.example.wishlist.data.entities.OfferingEntity
import com.example.wishlist.domain.models.Offering
import java.util.*

internal object TestFactory {

    fun createOfferingEntity(type: String = Offering.Type.VENUE.toString(),
                             price: String = "0.0",
                             date: String = "2021-06-15"): OfferingEntity {
        return OfferingEntity(id = "id", wished = true, type = type,
            name= "name", image = "image", currencyCode = "currencyCode", price = price,
            starsRating = Double.MAX_VALUE, location = "location", startDate = date, endDate = date)
    }

    fun createOfferingModel(id: String = "id",
                            wished: Boolean = true,
                            type: Offering.Type = Offering.Type.VENUE,
                            currencyCode: String = "EUR",
                            price: Double = 0.0,
                            starsRating: Double = Double.MAX_VALUE,
                            date: Date = Date()): Offering {
        return Offering(id = id, wished = wished, type = type,
            name= "name", image = "image", currencyCode = currencyCode, price = price,
            starsRating = starsRating, location = "location", startDate = date, endDate = date)
    }

}