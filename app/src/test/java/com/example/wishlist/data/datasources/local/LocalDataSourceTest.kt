package com.example.wishlist.data.datasources.local

import com.example.wishlist.data.entities.OfferingEntity
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
internal class LocalDataSourceTest {

    @Mock
    private lateinit var offeringDao: OfferingDao

    @InjectMocks
    private lateinit var subject: LocalDataSource

    @Test
    fun `When getting offering list, then a list is returned`() {
        val offerings = listOf(mock<OfferingEntity>())
        whenever(offeringDao.getOfferingList()).thenReturn(Single.just(offerings))

        subject.getOfferingList()
            .test()
            .assertNoErrors()
            .assertValue(offerings)
    }

    @Test
    fun `When getting offering list fails, then an error is returned`() {
        val exception = Exception()
        whenever(offeringDao.getOfferingList()).thenReturn(Single.error(exception))

        subject.getOfferingList()
            .test()
            .assertNoValues()
            .assertError(exception)
    }

    @Test
    fun `When getting wish offering list, then a list is returned`() {
        val offerings = listOf(mock<OfferingEntity>())
        whenever(offeringDao.getWishOfferingList()).thenReturn(Single.just(offerings))

        subject.getWishOfferingList()
            .test()
            .assertNoErrors()
            .assertValue(offerings)
    }

    @Test
    fun `When getting wish offering list fails, then an error is returned`() {
        val exception = Exception()
        whenever(offeringDao.getWishOfferingList()).thenReturn(Single.error(exception))

        subject.getWishOfferingList()
            .test()
            .assertNoValues()
            .assertError(exception)
    }

    @Test
    fun `When saving offering list, then a completable is returned`() {
        val offerings = listOf(mock<OfferingEntity>())
        whenever(offeringDao.saveOfferingList(offerings)).thenReturn(Completable.complete())

        subject.saveOfferingList(offerings)
            .test()
            .assertNoErrors()
            .assertComplete()
    }

    @Test
    fun `When saving offering fails, then an error is returned`() {
        val exception = Exception()
        val offerings = listOf(mock<OfferingEntity>())
        whenever(offeringDao.saveOfferingList(offerings)).thenReturn(Completable.error(exception))

        subject.saveOfferingList(offerings)
            .test()
            .assertNoValues()
            .assertError(exception)
    }

    @Test
    fun `When updating an offering, then a completable is returned`() {
        val offering = mock<OfferingEntity>()
        whenever(offeringDao.updateOffering(offering)).thenReturn(Completable.complete())

        subject.updateOffering(offering)
            .test()
            .assertNoErrors()
            .assertComplete()
    }

    @Test
    fun `When updating an offering fails, then an error is returned`() {
        val exception = Exception()
        val offering = mock<OfferingEntity>()
        whenever(offeringDao.updateOffering(offering)).thenReturn(Completable.error(exception))

        subject.updateOffering(offering)
            .test()
            .assertNoValues()
            .assertError(exception)
    }

    @Test
    fun `When updating an offering, then an update is emitted`() {
        val offering = mock<OfferingEntity>()
        whenever(offeringDao.updateOffering(offering)).thenReturn(Completable.complete())

        val updates = subject.getOfferingUpdates().test()

        subject.updateOffering(offering).test()

        updates.assertNoErrors()
            .assertValue(offering)
    }

}