package com.example.wishlist.data.repositories

import com.example.wishlist.data.datasources.local.LocalDataSource
import com.example.wishlist.data.datasources.network.NetworkDataSource
import com.example.wishlist.data.entities.OfferingEntity
import com.example.wishlist.data.mapper.OfferingMapper
import com.example.wishlist.domain.models.Offering
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
internal class RepositoryTest {

    @Mock
    private lateinit var networkDataSource: NetworkDataSource

    @Mock
    private lateinit var localDataSource: LocalDataSource

    @Mock
    private lateinit var mapper: OfferingMapper

    @Mock
    private lateinit var entity: OfferingEntity

    @Mock
    private lateinit var model: Offering

    @InjectMocks
    private lateinit var subject: Repository

    @Test
    fun `When fetching offerings on the network, then a list is returned and stored on database`() {
        val entities = listOf(entity)
        whenever(networkDataSource.getOfferingList()).thenReturn(Single.just(entities))
        whenever(localDataSource.saveOfferingList(entities)).thenReturn(Completable.complete())
        whenever(mapper.convert(entity)).thenReturn(model)

        subject.getOfferingList()
            .test()
            .assertNoErrors()
            .assertValue(listOf(model))
    }

    @Test
    fun `When fetching offerings on the network and it fails, then a list is returned from database`() {
        val entities = listOf(entity)
        whenever(networkDataSource.getOfferingList()).thenReturn(Single.error(Exception()))
        whenever(localDataSource.getOfferingList()).thenReturn(Single.just(entities))
        whenever(mapper.convert(entity)).thenReturn(model)

        subject.getOfferingList()
            .test()
            .assertNoErrors()
            .assertValue(listOf(model))
    }

    @Test
    fun `When fetching fails on both network and local, then an error is returned`() {
        val exception = Exception()
        whenever(networkDataSource.getOfferingList()).thenReturn(Single.error(exception))
        whenever(localDataSource.getOfferingList()).thenReturn(Single.error(exception))

        subject.getOfferingList()
            .test()
            .assertNoValues()
            .assertError(exception)
    }

    @Test
    fun `When fetching wish offerings, then it is returned from database`() {
        val entities = listOf(entity)
        whenever(localDataSource.getWishOfferingList()).thenReturn(Single.just(entities))
        whenever(mapper.convert(entity)).thenReturn(model)

        subject.getWishOfferingList()
            .test()
            .assertNoErrors()
            .assertValue(listOf(model))
    }

    @Test
    fun `When fetching wish offerings fails, then an error is returned`() {
        val exception = Exception()
        whenever(localDataSource.getWishOfferingList()).thenReturn(Single.error(exception))

        subject.getWishOfferingList()
            .test()
            .assertNoValues()
            .assertError(exception)
    }

    @Test
    fun `When updating a offering, then it updates on database`() {
        whenever(localDataSource.updateOffering(entity)).thenReturn(Completable.complete())
        whenever(mapper.convert(model)).thenReturn(entity)

        subject.updateOffering(model)
            .test()
            .assertNoErrors()
            .assertComplete()
    }

    @Test
    fun `When updating a offering fails, then an error is returned`() {
        val exception = Exception()
        whenever(localDataSource.updateOffering(entity)).thenReturn(Completable.error(exception))
        whenever(mapper.convert(model)).thenReturn(entity)

        subject.updateOffering(model)
            .test()
            .assertNoValues()
            .assertError(exception)
    }

    @Test
    fun `When getting offering updates, then it is returned`() {
        whenever(localDataSource.getOfferingUpdates()).thenReturn(Observable.just(entity))
        whenever(mapper.convert(entity)).thenReturn(model)

        subject.getOfferingUpdates()
            .test()
            .assertNoErrors()
            .assertValue(model)
    }

}