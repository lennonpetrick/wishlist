package com.example.wishlist.data.mapper

import com.example.wishlist.data.TestFactory
import com.example.wishlist.domain.models.Offering
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import java.text.SimpleDateFormat
import java.util.*

internal class OfferingMapperTest {

    private val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    private val subject = OfferingMapper()

    @Test
    fun `When converting an offering entity to a model, then it converts properly`() {
        val dateString = "2021-02-01"
        val entity = TestFactory.createOfferingEntity(type = "EXHIBITION",price = "19.50", date = dateString)
        val expectedDate = formatter.parse(dateString)

        val model = subject.convert(entity)

        assertAll("Offering fields", {
            with(model) {
                assertEquals(entity.id, id)
                assertEquals(entity.wished, wished)
                assertEquals(Offering.Type.EXHIBITION, type)
                assertEquals(entity.name, name)
                assertEquals(entity.image, image)
                assertEquals(entity.currencyCode, currencyCode)
                assertEquals(19.50, price)
                assertEquals(entity.starsRating, starsRating)
                assertEquals(entity.location, location)
                assertEquals(expectedDate, startDate)
                assertEquals(expectedDate, endDate)
            }
        })
    }

    @Test
    fun `When converting an offering model to an entity, then it converts properly`() {
        val date = Date(123456789)
        val model = TestFactory.createOfferingModel(type = Offering.Type.VENUE, price = 15.57, date = date)
        val expectedDateString = formatter.format(date)

        val entity = subject.convert(model)

        assertAll("Offering fields", {
            with(entity) {
                assertEquals(model.id, id)
                assertEquals(model.wished, wished)
                assertEquals("VENUE", type)
                assertEquals(model.name, name)
                assertEquals(model.image, image)
                assertEquals(model.currencyCode, currencyCode)
                assertEquals("15.57", price)
                assertEquals(model.starsRating, starsRating)
                assertEquals(model.location, location)
                assertEquals(expectedDateString, startDate)
                assertEquals(expectedDateString, endDate)
            }
        })
    }

}