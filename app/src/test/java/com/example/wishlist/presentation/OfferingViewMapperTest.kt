package com.example.wishlist.presentation

import com.example.wishlist.R
import com.example.wishlist.data.TestFactory
import com.example.wishlist.presentation.utils.DateFormatter
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class OfferingViewMapperTest {

    @Mock
    private lateinit var dateFormatter: DateFormatter

    @InjectMocks
    private lateinit var subject: OfferingViewMapper

    @Test
    fun `When converting an offering model to a view , then it converts properly`() {
        val date = Date()
        val model = TestFactory.createOfferingModel(
            currencyCode = "EUR",
            price = 23.43,
            starsRating = 4.5667,
            date = date
        )

        val expectedPrice = "€23.43"
        val expectedRating = "4.57 / 5"
        val expectedDate = "expectedDate"
        whenever(dateFormatter.formatDateRange(date, date)).thenReturn(expectedDate)

        val view = subject.convert(model)

        with(view) {
            assertEquals(model.id, id)
            assertEquals(model.name, name)
            assertEquals(model.image, image)
            assertEquals(expectedPrice, price)
            assertEquals(expectedRating, rating)
            assertEquals(model.location, location)
            assertEquals(expectedDate, dateRange)
        }
    }

    @Test
    fun `When offering wished is true, then it converts properly`() {
        val model = TestFactory.createOfferingModel(wished = true)

        val view = subject.convert(model)

        assertEquals(R.drawable.ic_bookmark_filled, view.wished)
    }

    @Test
    fun `When offering wished is false, then it converts properly`() {
        val model = TestFactory.createOfferingModel(wished = false)

        val view = subject.convert(model)

        assertEquals(R.drawable.ic_bookmark, view.wished)
    }

}