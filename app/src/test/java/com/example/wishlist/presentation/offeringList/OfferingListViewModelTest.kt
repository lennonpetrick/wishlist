package com.example.wishlist.presentation.offeringList

import com.example.wishlist.data.TestFactory
import com.example.wishlist.data.repositories.Repository
import com.example.wishlist.domain.models.Offering
import com.example.wishlist.domain.usecases.AddRemoveWishListUseCase
import com.example.wishlist.presentation.*
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class, InstantExecutorExtension::class)
internal class OfferingListViewModelTest {

    @Mock
    private lateinit var repository: Repository

    @Mock
    private lateinit var useCase: AddRemoveWishListUseCase

    @Mock
    private lateinit var mapper: OfferingViewMapper

    private lateinit var subject: OfferingListViewModel

    private val compositeDisposable = CompositeDisposable()
    private val scheduler = Schedulers.trampoline()

    @BeforeEach
    fun setUp() {
        subject = OfferingListViewModel(repository, useCase, mapper, scheduler, scheduler, compositeDisposable)
    }

    @Test
    fun `When onCreate is called, then the offering list is fetched`() {
        val offering = mock<Offering>()
        val offeringView = mock<OfferingView>()
        whenever(repository.getOfferingUpdates()).thenReturn(Observable.empty())
        whenever(repository.getOfferingList()).thenReturn(Single.just(listOf(offering)))
        whenever(mapper.convert(offering)).thenReturn(offeringView)

        val state = subject.observeStates<OfferingListState>()
        subject.onCreate(mock())

        state.assertValueAt(0) { it is OfferingListState.Loading }
            .assertLastValue {
                it is OfferingListState.OfferingList
                        && it.offerings == listOf(offeringView)
            }
    }

    @Test
    fun `When fetching offering list fails, then it returns an error`() {
        whenever(repository.getOfferingUpdates()).thenReturn(Observable.empty())
        whenever(repository.getOfferingList()).thenReturn(Single.error(Throwable()))

        val state = subject.observeStates<OfferingListState>()
        subject.onCreate(mock())

        state.assertValueAt(0) { it is OfferingListState.Loading }
            .assertLastValue{ it is OfferingListState.Error }
    }

    @Test
    fun `When onCreate is called, then it subscribes to offering updates`() {
        val offering = mock<Offering>()
        val offeringView = mock<OfferingView>()
        whenever(repository.getOfferingList()).thenReturn(Single.just(listOf(offering)))
        whenever(mapper.convert(offering)).thenReturn(offeringView)

        val offeringUpdated = mock<Offering>()
        val offeringViewUpdated = mock<OfferingView>()
        whenever(repository.getOfferingUpdates()).thenReturn(Observable.just(offeringUpdated))
        whenever(mapper.convert(offeringUpdated)).thenReturn(offeringViewUpdated)

        val state = subject.observeStates<OfferingListState>()
        subject.onCreate(mock())

        state.assertLastValue {
                it is OfferingListState.OfferingList
                        && it.offerings == listOf(offeringViewUpdated)
            }
    }

    @Test
    fun `When offering updates fails, then it returns an error`() {
        val offering = mock<Offering>()
        val offeringView = mock<OfferingView>()
        whenever(repository.getOfferingUpdates()).thenReturn(Observable.error(Throwable()))
        whenever(repository.getOfferingList()).thenReturn(Single.just(listOf(offering)))
        whenever(mapper.convert(offering)).thenReturn(offeringView)

        val state = subject.observeStates<OfferingListState>()
        subject.onCreate(mock())

        state.assertLastValue{ it is OfferingListState.Error }
    }

    @Test
    fun `When adding to wishlist or removing from wishlist, then the useCase is executed`() {
        val offering = TestFactory.createOfferingModel(id = "123")
        val offeringView = mock<OfferingView> { on { id } doReturn "123" }
        whenever(repository.getOfferingList()).thenReturn(Single.just(listOf(offering)))
        whenever(mapper.convert(offering)).thenReturn(offeringView)
        whenever(repository.getOfferingUpdates()).thenReturn(Observable.empty())
        whenever(useCase.run(offering)).thenReturn(Single.just(mock()))

        subject.onCreate(mock())
        subject.addRemoveWishList(offeringView)

        verify(useCase).run(offering)
    }

    @Test
    fun `When adding or removing fails, then it returns an error`() {
        val offering = TestFactory.createOfferingModel(id = "123")
        val offeringView = mock<OfferingView> { on { id } doReturn "123" }
        whenever(repository.getOfferingList()).thenReturn(Single.just(listOf(offering)))
        whenever(mapper.convert(offering)).thenReturn(offeringView)

        whenever(useCase.run(offering)).thenReturn(Single.error(Throwable()))

        whenever(repository.getOfferingUpdates()).thenReturn(Observable.empty())
        subject.onCreate(mock())

        val state = subject.observeStates<OfferingListState>()
        subject.addRemoveWishList(offeringView)

        state.assertLastValue { it is OfferingListState.Error }
    }

}