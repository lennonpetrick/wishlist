package com.example.wishlist.presentation.wishlist

import com.example.wishlist.data.TestFactory
import com.example.wishlist.data.repositories.Repository
import com.example.wishlist.domain.models.Offering
import com.example.wishlist.domain.usecases.AddRemoveWishListUseCase
import com.example.wishlist.presentation.*
import com.nhaarman.mockito_kotlin.*
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class, InstantExecutorExtension::class)
internal class WishListViewModelTest {

    @Mock
    private lateinit var repository: Repository

    @Mock
    private lateinit var useCase: AddRemoveWishListUseCase

    @Mock
    private lateinit var mapper: OfferingViewMapper

    private lateinit var subject: WishListViewModel

    private val compositeDisposable = CompositeDisposable()
    private val scheduler = Schedulers.trampoline()

    @BeforeEach
    fun setUp() {
        subject = WishListViewModel(repository, useCase, mapper, scheduler, scheduler, compositeDisposable)
    }

    @Test
    fun `When onCreate is called, then the wishlist is fetched`() {
        val offering = mock<Offering>()
        val offeringView = mock<OfferingView>()
        whenever(repository.getWishOfferingList()).thenReturn(Single.just(listOf(offering)))
        whenever(mapper.convert(offering)).thenReturn(offeringView)

        val state = subject.observeStates<WishListState>()
        subject.onCreate(mock())

        state.assertValueAt(0) { it is WishListState.Loading }
            .assertLastValue {
                it is WishListState.WishOfferingList
                        && it.offerings == listOf(offeringView)
            }
    }

    @Test
    fun `When fetching wishlist fails, then it returns an error`() {
        whenever(repository.getWishOfferingList()).thenReturn(Single.error(Throwable()))

        val state = subject.observeStates<WishListState>()
        subject.onCreate(mock())

        state.assertValueAt(0) { it is WishListState.Loading }
            .assertLastValue{ it is WishListState.Error }
    }

    @Test
    fun `When removing the offering from wishlist, then the list is re-fetched`() {
        val offering = TestFactory.createOfferingModel(id = "123")
        val offeringView = mock<OfferingView> { on { id } doReturn "123" }
        whenever(repository.getWishOfferingList()).thenReturn(Single.just(listOf(offering)))
        whenever(mapper.convert(offering)).thenReturn(offeringView)

        subject.onCreate(mock())

        whenever(useCase.run(offering)).thenReturn(Single.just(mock()))
        whenever(repository.getWishOfferingList()).thenReturn(Single.just(emptyList()))

        val state = subject.observeStates<WishListState>()
        subject.removeWishList(offeringView)

        state.assertLastValue {
            it is WishListState.WishOfferingList
                    && it.offerings.isEmpty()
        }
    }

    @Test
    fun `When removing from wishlist fails, then it returns an error`() {
        val offering = TestFactory.createOfferingModel(id = "123")
        val offeringView = mock<OfferingView> { on { id } doReturn "123" }
        whenever(repository.getWishOfferingList()).thenReturn(Single.just(listOf(offering)))
        whenever(mapper.convert(offering)).thenReturn(offeringView)

        subject.onCreate(mock())

        whenever(useCase.run(offering)).thenReturn(Single.error(Throwable()))

        val state = subject.observeStates<WishListState>()
        subject.removeWishList(offeringView)

        state.assertLastValue { it is WishListState.Error }
    }

    @Test
    fun `When removing from wishlist and the re-fetching fails, then it returns an error`() {
        val offering = TestFactory.createOfferingModel(id = "123")
        val offeringView = mock<OfferingView> { on { id } doReturn "123" }
        whenever(repository.getWishOfferingList()).thenReturn(Single.just(listOf(offering)))
        whenever(mapper.convert(offering)).thenReturn(offeringView)

        subject.onCreate(mock())

        whenever(useCase.run(offering)).thenReturn(Single.just(mock()))
        whenever(repository.getWishOfferingList()).thenReturn(Single.error(Throwable()))

        val state = subject.observeStates<WishListState>()
        subject.removeWishList(offeringView)

        state.assertLastValue { it is WishListState.Error }
    }

    @Test
    fun `When the removing method is called without fetching wishlist, then nothing is done`() {
        subject.removeWishList(mock())
        verify(useCase, never()).run(any())
    }

}